Composable Analytics Front-End Design Test
==========================================

If you're interested in working as a front-end engineer at Composable
Analytics, you can apply by completing this design test.

How to Apply
------------

1. [Sign up for a Bitbucket account](https://bitbucket.org/account/signup/) if
   you don't already have one.
2. [Fork this Bitbucket repository.](https://bitbucket.org/CA_Taymon/to-do-list/fork)
   Set the fork to be a private repository.
3. Complete the design test in your forked repository.  (You will need to
   install [Git](https://git-scm.com/) if you don't already have it, clone the
   forked repository onto your computer, make the changes there, commit them,
   and push them to the forked repository on Bitbucket.)
4. Make a pull request in Bitbucket from your forked repository to the original
   repository (CA_Taymon/to-do-list).  In the description for the pull request,
   include your name, email address, phone number, and a link to your résumé
   (this can just be a Dropbox or Google Drive link if you don't have a
   website).  Also include any links to your work that you think would help us
   see what you can do.  This information will remain private as long as your
   forked repository is a private repository.
5. We'll close the pull request when we're ready to tell you whether or not
   we're interested in moving forward with the interview process.

The Design Test
---------------

This repository contains a front-end for a simple Web-based to-do list app. To
run it, just open `todo.html` in a Web browser.  Since this is just a test for
front-end engineers, there's no back-end and all data is lost when the user
closes their browser window.

The user can create, delete, and edit tasks, and check them off when they're
done.  Tasks can be nested within other tasks.  In addition, the user interface
enforces the following rule: if a task is checked off, then all its subtasks
are also checked off.

Your assignment is to make this application more visually appealing and more
usable, to the best of your ability.

At the very least, you will need to write a CSS stylesheet, as the app
currently doesn't have one.  You may also modify or rewrite the HTML and/or
JavaScript code if doing so helps improve usability.  (The existing JavaScript
code is designed to be reasonably robust to changes in the structure of the
HTML, provided that the existing element identifiers and classes are left alone
and not used for any new elements.)

You may use any open source frameworks or libraries that you like.  (The
existing code uses only vanilla JavaScript.)

The app must render properly and run without bugs in the latest version of
Google Chrome, Mozilla Firefox, and Microsoft Edge.  (If you're not running
Windows 10, you can download a virtual machine with Microsoft Edge
[here](https://dev.modern.ie/tools/vms/windows/) and use it for testing.)
Other browsers need not be supported.

Good luck!